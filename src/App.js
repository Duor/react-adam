import React from 'react';
import './style/App.css';
import Nav from './components/Nav';
import Search from "./view/Search";
import Heroes from "./view/Heroes";
import heroDetail from './components/heroDetail';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App() {
    return (
        <Router>
            <div className="App">
                <Nav/>
                <Switch>
                    <Route path="/" exact component={Home}/>
                    <Route path="/search" exact component={Search}/>
                    <Route path="/heroes" exact component={Heroes}/>
                    <Route path="/hero/:id" component={heroDetail}/>
                </Switch>
            </div>
        </Router>
    );
}

const Home = () => (
    <div>
        <h1>Home Page</h1>
    </div>
)

export default App;
