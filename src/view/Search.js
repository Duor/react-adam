import React, {useEffect, useState} from 'react';
import '../style/search.css';
import search from '../rsc/search.png';
import ReactDOM from "react-dom";

function Search() {

    const [searchResults, setSearchResults] = useState([]);



    const fetchResults = async () => {
        const urlName = "https://superheroapi.com/api/3061607853876230/search/name/";
        const searchValue = (document.getElementById("search-txt").value);
        const hero = await (await fetch(
            "https://superheroapi.com/api/3061607853876230/search/name/"
            + "Batman", {mode: 'no-cors'}).then(hero => hero.json()))



        console.log(hero);

    }


    return (
        <div className="S-app">
            <div className="search-bg"></div>
            <div className="search-box">
                <input
                    id="search-txt"
                    type="text"
                    placeholder="Search your favorite hero"
                />
                <img
                    className="search-icon"
                    src={search}
                    onClick={fetchResults}></img>
            </div>
        </div>
    );
}


export default Search;